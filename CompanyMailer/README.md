# Company Mailer Project

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Features](#features)
* [Sources](#sources)

## General info
A mailer application for a company that can be used by the employees 
of the company only. It can be run on internet. Each user can send mails, 
receive mails and delete mails after getting logged in!

## Technologies
Project is created with:
* Java 8
* Maven 3.6
* Servlet 3.1
* PostgreSQL 12
* Tomcat 9.0

 ## Setup
 1. Create database and user for them.
 2. Run DDL SQL script from **src/main/db/db.sql** to create tables.
 3. Package project use command *mvn package*.
 4. Run result **.war** file on Tomcat or other Java Servlet implementation.
 
 ## Features
 User can
 * Can register
 * Can login and logout
 * Can send mails
 * Can see inbox
 * Can see sent mails
 * Can see trash
 * Can search other employees email id
 * Can view and update profile
 
 ## Sources
 This app based on [Open Source application](https://www.javatpoint.com/company-mailer-servlet-project)
  
 ### Difference with the original
 * Added maven
 * Changed DBMS to PostgreSQL
 * Changed Connection to Connection Pool
 * 