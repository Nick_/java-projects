CREATE TABLE  "company_mailer_user"
(
    id                SERIAL,
    name              VARCHAR(4000),
    email             VARCHAR(4000),
    password          VARCHAR(4000),
    gender            VARCHAR(4000),
    dob               DATE,
    address_line      VARCHAR(4000),
    city              VARCHAR(4000),
    state             VARCHAR(4000),
    country           VARCHAR(4000),
    contact           VARCHAR(4000),
    registered_date   DATE,
    authorized        VARCHAR(4000),
    CONSTRAINT        pk_company_mailer_user PRIMARY KEY (id)
);

CREATE TABLE  "company_mailer_message"
(
    id            SERIAL,
    sender        VARCHAR(4000),
    receiver      VARCHAR(4000),
    subject       VARCHAR(4000),
    message       VARCHAR(4000),
    trash         VARCHAR(4000),
    message_date  DATE,
    CONSTRAINT    pk_company_mailer_message PRIMARY KEY (id)
);