package xyz.nick.servlet;

import xyz.nick.db.ConnectionProvider;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/mailer/send")
public class SentServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		request.getRequestDispatcher("/header.html").include(request, response);
		request.getRequestDispatcher("/link.html").include(request, response);
		
		HttpSession session=request.getSession(false);
		/*if(session==null){
			response.sendRedirect("index.html");
		}else{*/
			String email=(String)session.getAttribute("email");
			out.print("<span style='float:right'>Hi, "+email+"</span>");
			out.print("<h1>Sent Mail</h1>");

			try (Connection con = ConnectionProvider.getConnection()) {
				PreparedStatement ps=con.prepareStatement("select * from company_mailer_message where sender=? and trash='no' order by id desc");
				ps.setString(1,email);
				ResultSet rs=ps.executeQuery();
				out.print("<table border='1' style='width:700px;'>");
				out.print("<tr style='background-color:grey;color:white'><td>To</td><td>Subject</td></tr>");
				while(rs.next()){
					out.print("<tr><td>"+rs.getString("receiver")+"</td><td><a href='mailer/mail/view?id="+rs.getString(1)+"'>"+rs.getString("subject")+"</a></td></tr>");
				}
				out.print("</table>");

			}catch(Exception e){out.print(e);}
//		}
		
		
		
		request.getRequestDispatcher("/footer.html").include(request, response);
		out.close();

	}

}
