package xyz.nick.db;

import org.postgresql.ds.PGSimpleDataSource;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.Objects;
import java.util.Properties;

public class ConnectionProvider {

	private static PGSimpleDataSource ds;

	private static PGSimpleDataSource getDataSource() {

		if (ds == null) {
			Properties props = new Properties();
			FileInputStream fis;
			try {
				ClassLoader classLoader = ConnectionProvider.class.getClassLoader();
				fis = new FileInputStream(Objects
                        .requireNonNull(classLoader.getResource("db.properties"))
                        .getFile());
				props.load(fis);
				ds = new PGSimpleDataSource() ;
				ds.setServerName(props.getProperty("db.url"));
				ds.setDatabaseName(props.getProperty("db.database"));
				ds.setUser(props.getProperty("db.user"));
				ds.setPassword(props.getProperty("db.password"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return ds;
	}

	public static Connection getConnection() {
		Connection con = null;
		try {
			con = getDataSource().getConnection();
		} catch (Exception e){
			e.printStackTrace();
		}
		return con;
    }
}
